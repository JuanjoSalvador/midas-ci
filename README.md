# Midas CI
## Sistema minimalista de integración contínua (CI)

Midas CI es un sistema minimalista de integración contínua diseñado para ejecutarse sobre una Raspberry Pi 2 Modelo B (o superior), que se integra con tu servicio de Git preferido.

Se configura mediante un fichero `tasks.yml` en la raíz de tu proyecto.

```yaml
tasks:
  test:
    run: python tests.py

  build:
    run: sh build_script.sh
```

Midas CI se puede utilizar en conjunto con interfaz web e integración con otros servicios, o solo el gestor de tareas como aplicación standalone.

Ejemplo de ejecución por separado.

```
(venv) [~/midas-ci] [master *] → python3 runner/runner.py --file tasks.yml
## Running test
hello world!
> SUCCESS
## Running build
sh: 0: Can't open build.sh
> FAILED
```

### Licencia

> MIT License
>
>Copyright (c) 2019 Juanjo Salvador
>
>Permission is hereby granted, free of charge, to any person obtaining a copy
>of this software and associated documentation files (the "Software"), to deal
>in the Software without restriction, including without limitation the rights
>to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
>copies of the Software, and to permit persons to whom the Software is
>furnished to do so, subject to the following conditions:
>
>The above copyright notice and this permission notice shall be included in all
>copies or substantial portions of the Software.
>
>THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
>IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
>FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
>AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
>LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
>OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
>SOFTWARE.
