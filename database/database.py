import sqlite3
import configparser
import os
from pathlib import Path

config = configparser.ConfigParser()
config.read(os.path.join(os.path.dirname(__file__), 'settings.ini'))

DATABASE_PATH  = config['DEFAULT']['DATABASE_PATH']
DATABASE_STORE = config['DEFAULT']['DATABASE_STORE']

def connectDB():
  try:
    conn = sqlite3.connect(DATABASE_PATH)
  except Exception as ex:
    pass


def create():
    pass

def read():
    pass

def update():
    pass

def delete():
    pass