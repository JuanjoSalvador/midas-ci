-- Activate FKs
PRAGMA foreign_keys = 1;

-- Create table Projects
CREATE TABLE projects (
    ProjectName TEXT PRIMARY KEY,
    LastCommit TEXT
);

-- Create table Tasks
CREATE TABLE tasks (
    Date TEXT NOT NULL,
    TaskName TEXT NOT NULL,
    TaskStatus TEXT NOT NULL,
    Project TEXT PRIMARY KEY,
    FOREIGN KEY(Project) REFERENCES projects(ProjectName)
);