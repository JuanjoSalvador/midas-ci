from flask import Flask, request, render_template, redirect
from flask import send_from_directory

import database
import runner

app = Flask(__name__, 
        static_url_path='',
        static_folder='statics',
        template_folder='templates')

@app.route('/',methods=['GET'])
def root():
    return render_template("index.jinja")

@app.route('/docs', methods=['GET'])
def docs():
    return redirect("https://gitlab.com/JuanjoSalvador/midas-ci/wikis/home")

@app.route('/new',methods=['GET', 'POST'])
def new_project():
    if request.method == 'GET':
        return render_template("new.jinja")

    elif request.method == 'POST':
        fname = request.form["project_name"]
        frepo = request.form["project_repo"]
        
        db_insert_success = True

        return render_template("new.jinja", name = fname, repo = frepo, db_insert_success = db_insert_success)

@app.route('/view/<project>/',methods=['GET'])
def view_project(project):
    return render_template("project.jinja", project = project)


# Webhooks
@app.route('/webhook/<id>/push',methods=['POST'])
def push():
    return "OK"

if __name__ == '__main__':
    app.run()
