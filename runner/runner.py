import yaml
import subprocess
import sqlite3
import argparse
import configparser
import os

from pathlib import Path
from termcolor import colored
from datetime import datetime

config = configparser.ConfigParser()
config.read(os.path.join(os.path.dirname(__file__), 'settings.ini'))

DATABASE_PATH  = config['DEFAULT']['DATABASE_PATH']
DATABASE_STORE = config['DEFAULT']['DATABASE_STORE']

def connect_database():
    database = Path(DATABASE_PATH)
    if not database.exists():
        conn = sqlite3.connect(DATABASE_PATH)
        c = conn.cursor()
        c.execute("CREATE TABLE tasks (date text, task_name text, task_status text)")
        conn.commit()
        conn.close()

def insert_database(date, task_name, task_status):
    connect_database()
    conn = sqlite3.connect(DATABASE_PATH)
    c = conn.cursor()
    c.execute("INSERT INTO tasks VALUES ('{}', '{}', '{}')".format(date, task_name, task_status))
    conn.commit()
    conn.close()

def logger(date, task, log):
    path = os.path.dirname(os.path.realpath(__file__))
    file_name = "{}/logs/{}.{}.log".format(path, task, date)
    log_file = open(file_name, 'w+')
    log_file.write(log.decode("utf-8"))
    log_file.close()

def run(name = None, command = None):
    print(colored("## Running {}".format(name), 'blue'))
    date = datetime.now().strftime("%d%m%Y%H%M%S")
    p = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    if p.returncode == 0:
        print(p.stdout.decode().rstrip())
        print(colored("> SUCCESS", 'green'))
        logger(date, name, p.stdout)

        if DATABASE_STORE:
            insert_database(date, name, "success")
    else:
        print(p.stderr.decode().rstrip())
        print(colored("> FAILED", 'red'))
        logger(date, name, p.stderr)

        if DATABASE_STORE:
            insert_database(date, name, "fail")

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--file", help="Fichero tasks.yml con tareas a ejecutar (opcional)",  nargs='?')
    parser.add_argument("-t", "--task", help="Tarea a ejecutar",  action="store")
    args = parser.parse_args()

    taskfile = args.file
    task = args.task

    if not taskfile:
        taskfile = 'tasks.yml'

    try:
        with open(taskfile, 'r') as stream:
            try:
                tasks = yaml.safe_load(stream)

                if not task:
                    for name, command in tasks['tasks'].items():
                        run(name, command)
                else:
                    for name, command in tasks['tasks'][task].items():
                        run(name, command)

            except yaml.YAMLError as exc:
                print(exc)
    except Exception as ex:
        print(ex)

if __name__ == "__main__":
    main()